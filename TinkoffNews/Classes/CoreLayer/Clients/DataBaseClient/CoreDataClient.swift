//
//  Created by Максим Гришутин on 26.03.2018.
//  Copyright © 2018 Максим Гришутин. All rights reserved.
//

import Foundation
import CoreData

class CoreDataClient {

    static let shared = CoreDataClient()

    var defaultFetchBatchSize = 20

    lazy var context: NSManagedObjectContext = {
        return self.persistentContainer.viewContext
    }()

    lazy var persistentContainer: NSPersistentContainer = {
        let projectName = Bundle.main.infoDictionary!["CFBundleName"] as! String
        let container = NSPersistentContainer(name: projectName)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        guard let modelURL = Bundle.main.url(forResource: "TinkoffNews", withExtension: "momd"),
            let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Failed to created managed object model")
        }

        return managedObjectModel
    }()

    func saveContext(completion: (() -> ())? = nil) {
        if self.context.hasChanges {
            do {
                try self.context.save()
                if let c = completion { c() }
            }
            catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func delete(objects: [NSManagedObject]) {
        for object in objects {
            context.delete(object)
        }
    }

    func deleteAllObjects() {
        for entityName in managedObjectModel.entitiesByName.keys {

            let request = NSFetchRequest<NSManagedObject>(entityName: entityName)
            request.includesPropertyValues = false

            do {
                for object in try context.fetch(request) {
                    context.delete(object)
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }

    func fetchObject<T: NSManagedObject>(entity: T.Type, predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor]? = nil, context: NSManagedObjectContext) -> T? {

        let request = NSFetchRequest<T>(entityName: String(describing: entity))

        request.predicate = predicate
        request.sortDescriptors = sortDescriptors
        request.fetchLimit = 1

        do {
            return try context.fetch(request).first
        }
        catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
    }

    func fetchObjects<T: NSManagedObject>(entity: T.Type, predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor]? = nil) -> [T] {

        let request = NSFetchRequest<T>(entityName: String(describing: entity))
        request.predicate = predicate
        request.sortDescriptors = sortDescriptors
        request.fetchBatchSize = defaultFetchBatchSize

        do {
            return try context.fetch(request)
        }
        catch let error as NSError {
            print(error.localizedDescription)
            return [T]()
        }
    }

}
