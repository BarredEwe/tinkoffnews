//
//  Created by Максим Гришутин on 26.03.2018.
//  Copyright © 2018 Максим Гришутин. All rights reserved.
//

import Foundation

protocol NetworkClient {

}

public enum Result<T> {
    case success(T)
    case failure(String)
}
