//
//  Created by Максим Гришутин on 26.03.2018.
//  Copyright © 2018 Максим Гришутин. All rights reserved.
//

import Foundation

class RestNetworkClient: NSObject {

    func request<T: Decodable>(with urlString: String, completion: @escaping (Result<T>) -> Void) {

        guard let url = URL(string: urlString) else {
            return completion(.failure("Invalid URL, we can't update your feed"))
        }

        var request = URLRequest(url: url)
        request.httpMethod = "GET"

        URLSession.shared.dataTask(with: request) { (data, response, error) in

            guard error == nil else { return completion(.failure(error!.localizedDescription)) }
            guard let data = data  else { return completion(.failure(error?.localizedDescription ?? "There are no new Items to show"))
            }

            do {
                if let json =  try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [String: AnyObject],
                    let data = json["payload"] as? [[String: AnyObject]] {
                    let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                    let news = try JSONDecoder().decode(T.self, from: jsonData)
                    completion(.success(news))
                }
//                if let json = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [String: AnyObject],
//                    let data = json["payload"] as? [[String: AnyObject]] {
//                        data.forEach { (item:[String:AnyObject]) in
//                            if let id = item["id"] as? String,
//                                let bankInfoTypeId = item["bankInfoTypeId"] as? Int32,
//                                let name = item["name"] as? String,
//                                let text = item["text"] as? String,
//                                let publicationDate = item["publicationDate"] as? [String: AnyObject],
//                                let dateTimeMiliSec = publicationDate["milliseconds"] as? Double {
//                                let news = CoreDataDBClient.instance.newManagedObject as! News
//                                news.uid = id
//                                news.bankInfoTypeId = bankInfoTypeId
//                                news.text = text
//                                news.publicationDate = dateTimeMiliSec
//                                news.name = name
//                                print(news)
//                                CoreDataDBClient.instance.saveContext()
//                            }
//                        }
//                }

//                                if let json = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [String: AnyObject] {

//                let encoder = JSONEncoder()
//                encoder.outputFormatting = .prettyPrinted
//
//                let data = try encoder.encode(<#T##value: Encodable##Encodable#>)

//                if let json = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [String: AnyObject] {
//                    guard let itemsJsonArray = json["items"] as? [[String: AnyObject]] else {
//                        return completion(.Error(error?.localizedDescription ?? "There are no new Items to show"))
//                    }
//                    DispatchQueue.main.async {
//                        completion(.Success(itemsJsonArray))
//                    }
//                }
            } catch let error {
                print(error.localizedDescription)
                return completion(.failure(error.localizedDescription))
            }
            }.resume()
    }
}
