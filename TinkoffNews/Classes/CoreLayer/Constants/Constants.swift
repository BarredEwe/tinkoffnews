//
//  Created by Максим Гришутин on 27.03.2018.
//  Copyright © 2018 Максим Гришутин. All rights reserved.
//

import Foundation

struct Constants {
    static let serverUrlPath = "https://api.tinkoff.ru/v1/"
    static let newsUrlPath = serverUrlPath + "news"
    static let detailNewsUrlPath = serverUrlPath + "news_content"
}
