//
//  Created by Максим Гришутин on 27.03.2018.
//  Copyright © 2018 Максим Гришутин. All rights reserved.
//

import Foundation
import CoreData

class CoreDataRepository<T>: Repository where T: CoreDataEntity, T: NSManagedObject, T.EntityType: Entity {

    typealias EntityType = T

    let coreDataClient = CoreDataClient.shared

    var entity: NSEntityDescription {
        return NSEntityDescription.entity(forEntityName: String(describing: T.self), in: coreDataClient.context)!
    }

    var newManagedObject: NSManagedObject {
        return NSManagedObject(entity: self.entity, insertInto: coreDataClient.context)
    }

    func fetchObjects(first: Int, last: Int, predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor]? = nil) -> [T.EntityType] {
        let objects = coreDataClient.fetchObjects(entity: T.self, predicate: predicate,
                                                  sortDescriptors: sortDescriptors)
        return objects[first...last].flatMap {
            $0.plainObject
        }
    }

    func save(items: [T.EntityType], _ completion: (() -> ())? = nil) {
        let coreDataItems = items.flatMap {
            $0.modelObject
        }
        print(coreDataItems)
        coreDataClient.saveContext(completion: completion)
    }

    func delete(predicate: NSPredicate) {
        let objects = coreDataClient.fetchObjects(entity: T.self, predicate: predicate,
                                   sortDescriptors: nil)

        coreDataClient.delete(objects: objects as [NSManagedObject]);
        coreDataClient.saveContext()
    }


//    typealias RealmEntityType = T
//
//    private let fileURL: URL
//    private let realm: Realm
//
//    init() {
//        fileURL = FileManager.default
//            .containerURL(forSecurityApplicationGroupIdentifier: "group.push.mobile")!
//            .appendingPathComponent("db.realm")
//
//        var configuration = Realm.Configuration(fileURL: fileURL)
//        configuration.objectTypes = [NotificationModelObject.self, SenderInfoModelObject.self]
//        let folderPath = configuration.fileURL!.deletingLastPathComponent().path
//
//        // Disable file protection for this directory
//        let attributes = [FileAttributeKey.protectionKey: FileProtectionType.none]
//        try! FileManager.default.setAttributes(attributes, ofItemAtPath: folderPath)
//
//        Realm.Configuration.defaultConfiguration = configuration
//        self.realm = try! Realm()
//    }
//
//    func save(item: T.EntityType) throws {
//        try realm.write {
//            realm.add(item.modelObject as! T)
//        }
//    }
//
//    func save(items: [T.EntityType]) throws {
//        try realm.write {
//            items.forEach {
//                realm.add($0.modelObject as! T, update: true)
//            }
//        }
//    }
//
//    func update(block: @escaping () -> Void) throws {
//        try realm.write {
//            block()
//        }
//    }
//
//    func delete<C: Object>(item: C) throws {
//        try realm.write {
//            realm.delete(item)
//        }
//    }
//
//    func delete(item: T.EntityType) throws {
//        try realm.write {
//            realm.delete(item.modelObject as! T)
//        }
//    }
//
//    func deleteAll() throws {
//        try realm.write {
//            realm.delete(realm.objects(T.self))
//        }
//    }
//
//    func fetch(predicate: NSPredicate?) -> [T]? {
//        var objects = realm.objects(T.self)
//
//        if let predicate = predicate {
//            objects = objects.filter(predicate)
//        }
//
//        return objects.flatMap { $0 }
//    }
//
//    func fetch(predicate: NSPredicate?, sorted: Sorted?) -> [T.EntityType] {
//        return fetch(predicate: predicate, sorted: sorted, page: nil)
//    }
//
//    func fetch(predicate: NSPredicate?, sorted: Sorted?, page: (limit: Int, offset: Int)?) -> [T.EntityType] {
//        var objects = realm.objects(T.self)
//
//        if let predicate = predicate {
//            objects = objects.filter(predicate)
//        }
//
//        if let sorted = sorted {
//            objects = objects.sorted(byKeyPath: sorted.key, ascending: sorted.ascending)
//        }
//
//        if let page = page {
//            var limit = 0
//            if objects.count > page.offset + page.limit {
//                limit = page.offset + page.limit
//            } else {
//                limit = objects.count
//            }
//            return objects[page.offset..<limit].flatMap {
//                $0.plainObject
//            }
//        }
//
//        return objects.flatMap {
//            $0.plainObject
//        }
//    }
//
//    func fetchAll() -> [T.EntityType] {
//        return realm.objects(T.self).flatMap {
//            $0.plainObject
//        }
//    }
}
