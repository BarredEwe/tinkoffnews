//
//  Created by Максим Гришутин on 27.03.2018.
//  Copyright © 2018 Максим Гришутин. All rights reserved.
//

import Foundation

protocol Entity {
    associatedtype CoreDataEntityType
    var modelObject: CoreDataEntityType { get }
}

protocol CoreDataEntity {
    associatedtype EntityType
    var plainObject: EntityType { get }
}
