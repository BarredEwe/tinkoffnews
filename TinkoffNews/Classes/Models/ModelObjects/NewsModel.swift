//
//  Created by Максим Гришутин on 27.03.2018.
//  Copyright © 2018 Максим Гришутин. All rights reserved.
//

import Foundation
import CoreData

@objc(NewsModel)
class NewsModel: NSManagedObject, CoreDataEntity {
    typealias EntityType = News

    @NSManaged public var bankInfoTypeId: Int32
    @NSManaged public var id: String!
    @NSManaged public var name: String!
    @NSManaged public var publicationDate: Double
    @NSManaged public var text: String!
    @NSManaged public var countShow: Int32

    var plainObject: News {
        return News(bankInfoTypeId: bankInfoTypeId, id: id, name: name, publicationDate: PublicationDate(milliseconds: publicationDate), text: text, countShow: countShow)
    }
}
