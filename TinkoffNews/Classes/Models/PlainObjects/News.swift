//
//  Created by Максим Гришутин on 27.03.2018.
//  Copyright © 2018 Максим Гришутин. All rights reserved.
//

import Foundation

struct PublicationDate: Decodable {
    var milliseconds: Double
}

struct News: Entity, Decodable, Equatable {

    typealias CoreDataEntityType = NewsModel

    var bankInfoTypeId: Int32
    var id: String
    var name: String!
    var publicationDate: PublicationDate!
    var text: String!
    var countShow: Int32?

    var modelObject: NewsModel {
        let news = CoreDataRepository<NewsModel>().newManagedObject as! NewsModel
        news.bankInfoTypeId = bankInfoTypeId
        news.id = id
        news.name = name
        news.publicationDate = publicationDate.milliseconds
        news.text = text
        news.countShow = countShow ?? 0
        return news
    }

    static func ==(lhs: News, rhs: News) -> Bool {
        return lhs.id == rhs.id
    }

}
