//
//  Created by Максим Гришутин on 27.03.2018.
//  Copyright © 2018 Максим Гришутин. All rights reserved.
//

import Foundation

class NewsNetworkService: NSObject {

    private let networkClient = RestNetworkClient()

    func requestNews(with firstIndex: Int, lastIndex: Int, _ completion: @escaping (Result<[News]>) -> Void) {
        networkClient.request(with: Constants.newsUrlPath + "?first=\(firstIndex)&last=\(lastIndex)",
            completion: completion)
    }

}
