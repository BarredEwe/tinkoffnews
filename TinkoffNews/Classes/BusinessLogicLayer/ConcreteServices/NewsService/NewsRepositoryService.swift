//
//  Created by Максим Гришутин on 27.03.2018.
//  Copyright © 2018 Максим Гришутин. All rights reserved.
//

import Foundation

class NewsRepositoryService: NSObject {

    func loadNews(with firstIndex: Int, lastIndex:Int) -> [News] {
        let sort = NSSortDescriptor(key: "publicationDate", ascending: false)
        return CoreDataRepository<NewsModel>().fetchObjects(first: firstIndex, last: lastIndex, predicate: nil, sortDescriptors: [sort])
    }

    func save(news: [News], _ completion: (() -> ())? = nil) {
        CoreDataRepository<NewsModel>().save(items: news, completion)
    }

    func delete(news: [News]) {
        if !news.isEmpty {
            let predicate = NSPredicate(format: "id IN %@", news.map({ $0.id }))
            CoreDataRepository<NewsModel>().delete(predicate: predicate)
        }
    }

}
