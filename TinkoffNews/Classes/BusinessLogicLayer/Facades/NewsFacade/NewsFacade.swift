//
//  Created by Максим Гришутин on 27.03.2018.
//  Copyright © 2018 Максим Гришутин. All rights reserved.
//

import Foundation

class NewsFacade: NSObject {

    static let shared = NewsFacade()

    private let networkService = NewsNetworkService()
    private let repositoryService = NewsRepositoryService()

    func getNews(with firstIndex: Int, lastIndex: Int, _ completion: @escaping (Result<[News]>) -> Void) {
        let newsCoreData = repositoryService.loadNews(with: firstIndex, lastIndex: firstIndex)
        completion(.success(newsCoreData))

        networkService.requestNews(with: firstIndex, lastIndex: lastIndex) { result in
            switch result {
            case let .success(news):
                if !(news == newsCoreData) {
                    self.repositoryService.delete(news: newsCoreData)
                    self.repositoryService.save(news: news, {
                        completion(.success(news))
                    })
                }
            case let .failure(error):
                print(error)
                completion(.failure(error))
            }
        }
    }

}
