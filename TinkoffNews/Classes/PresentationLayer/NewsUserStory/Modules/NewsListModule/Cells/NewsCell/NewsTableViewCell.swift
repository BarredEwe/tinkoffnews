//
//  Created by Максим Гришутин on 26.03.2018.
//  Copyright © 2018 Максим Гришутин. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var dataLabel: UILabel!

    @IBOutlet weak var contentLabel: UILabel!

    @IBOutlet weak var countLabel: UILabel!

    @IBOutlet weak var countView: UIView!

    func configure(with news: News) {
        dataLabel.text = "\(news.publicationDate.milliseconds)"
        contentLabel.text = news.text
        countView.isHidden = news.countShow ?? 0 == 0
        countLabel.text = String(news.countShow ?? 0)
    }

}
