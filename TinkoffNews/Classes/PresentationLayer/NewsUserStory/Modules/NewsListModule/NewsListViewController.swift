//
//  Created by Максим Гришутин on 26.03.2018.
//  Copyright © 2018 Максим Гришутин. All rights reserved.
//

import UIKit

class NewsListViewController: UIViewController {

    private let service = NewsFacade()

    private var news = [News]()

    private let defaultPageSize = 20

    @IBOutlet private var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView()
            tableView.refreshControl = UIRefreshControl()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.refreshControl?.addTarget(self, action: #selector(didChangedRefresh), for: .valueChanged)
        DispatchQueue.global().async {
            self.loadNews(force: true)
        }
    }

    // MARK: Private
    @objc private func didChangedRefresh() {
        loadNews(force: true)
    }

    private func loadNews(force: Bool) {
        service.getNews(with: force ? 0 : news.count,
                        lastIndex: force ? defaultPageSize : news.count + defaultPageSize) { result in
            switch result {
            case let .success(news):
                if self.news == news {
                    DispatchQueue.main.async {
                        self.tableView.refreshControl?.endRefreshing()
                    }
                    return
                }
                self.news = force ? news : self.news + news
                DispatchQueue.main.async {
                    self.tableView.refreshControl?.endRefreshing()
                    self.tableView.reloadData()
                }
            case let .failure(error):
                //TODO: show error
                print(error)
                DispatchQueue.main.async {
                    self.tableView.refreshControl?.endRefreshing()
                }
            }
        }
    }

}

// MARK: UITableViewDataSource
extension NewsListViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "newsCell") as! NewsTableViewCell
        cell.configure(with: news[indexPath.row])
        return cell
    }
}

extension NewsListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        news[indexPath.row].countShow = 1
        tableView.reloadRows(at: [indexPath], with: .automatic)

        //TODO: Show detail Information
    }
}
